# card10 launcher

A nice launcher / menu for card10.


## Installation

Copy the `menu.py` and the `launcher` directory to the root directory of your card10. This will replace the existing
menu.


## Configuration

The launcher can be configured via a `launcher.json` placed in the root directory of your card10. See the included
`launcher.json` for example values.

The following options are supported:

* `"use_icons"`: Set this to `false` to not show icons for apps but instead display the app name.
* `"show_home"`: Set this to `false` to not show `Home` as an app in the launcher. 
* `"app_themes"`: An object (key-value-map) of themes for specific apps (see below for how to specify a theme). As
  keys use the **exact** display names of the apps you would like to theme. E.g.: To change the theme for the cyber app
  you have to use `"CYBER!!1einself!"` as the key. 
* `"default_themes"`: A list of default themes (see below for how to specify a theme) to apply for apps. The first app
  will get the first theme, the second app the second theme and so on. If the end of the list is reached the next app
  will get the first theme again and so on. 


### App themes

An app theme is specified as a JSON object. The following fields are supported:

* `"fg"`: Foreground color - used for text or icon. Supported are HTML-like RGB color values, e.g.: `#005383`.
* `"bg"`: Background color. Supported are HTML-like RGB color values, e.g.: `#005383`.
* `"icon"`: The name of the icon to use. Possible icon names are e.g.: `"bluetooth"`, `"cyber"`, `"ecg"`,
  `"digi_clock"`, `"fairydust_communication"`. For a complete list of icons look at the directory `/launcher/icons`.
  For each icon there is a corresponding file named `[name].py` (except `__init__` is **not** a valid icon name).


## Uninstall

Delete the `menu.py`, `launcher.json` and the `launcher` directory in the root directory of your card10.


