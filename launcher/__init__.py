import sys

from launcher.apps import enumerate_apps
from launcher.config import LauncherConfig
from launcher.menu import MainMenu


def load_config():
    try:
        return LauncherConfig.from_file('/launcher.json')
    except Exception as e:
        print('Failed loading launcher config.')
        sys.print_exception(e)
        return LauncherConfig.default()


def run():
    MainMenu(enumerate_apps(), load_config()).run()


if __name__ == "__main__":
    run()
