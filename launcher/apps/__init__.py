import os
import sys

import ujson


class AppTheme:
    def __init__(self, icon=None, fg=None, bg=None):
        self.icon = icon
        self.fg = fg
        self.bg = bg


class App:
    def __init__(self, name, path, is_home=False, theme=AppTheme()):
        self.name = name
        self.path = path
        self.is_home = is_home
        self.theme = theme

    def set_theme(self, theme):
        self.theme = theme


class HomeApp(App):
    def __init__(self):
        super().__init__('Home', 'main.py', is_home=True)


def enumerate_apps():
    """List all installed apps."""
    for f in os.listdir("/"):
        if f == "main.py":
            yield HomeApp()

    for app in sorted(os.listdir("/apps")):
        if app.startswith("."):
            continue

        if app.endswith(".py") or app.endswith(".elf"):
            yield App(app, "/apps/" + app)
            continue

        try:
            with open("/apps/" + app + "/metadata.json") as f:
                info = ujson.load(f)

            yield App(
                info["name"], "/apps/{}/{}".format(app, info.get("bin", "__init__.py"))  #
            )
        except Exception as e:
            print("Could not load /apps/{}/metadata.json!".format(app))
            sys.print_exception(e)
