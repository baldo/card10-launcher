import color
import ujson
import ure

from launcher.apps import AppTheme
from launcher.icons import ICONS


# TODO: Read from JSON and add sensible fallback.
class LauncherConfig:
    _HTML_COLOR_REGEXP = ure.compile('^#[0-9a-fA-F]+$')

    def __init__(self, use_icons=True, show_home=True, app_themes=None, default_themes=None):
        self.use_icons = use_icons
        self.show_home = show_home
        self._app_themes = app_themes or {}

        self._default_themes = default_themes
        if default_themes is None:
            self._default_themes = [
                AppTheme(fg=color.WHITE, bg=color.CHAOSBLUE_DARK),
                AppTheme(fg=color.WHITE, bg=color.CAMPGREEN_DARK),
                AppTheme(fg=color.WHITE, bg=color.COMMYELLOW_DARK),
            ]

        self._default_theme_index = 0

    def apply_theme(self, app):
        # override default themes by config
        if app.name in self._app_themes.keys():
            app.set_theme(self._app_themes[app.name])

        else:
            app.set_theme(self._next_default_theme())

    def _next_default_theme(self):
        theme = self._default_themes[self._default_theme_index]
        self._default_theme_index = (self._default_theme_index + 1) % len(self._default_themes)
        return theme

    @classmethod
    def from_file(cls, path):
        with open(path) as f:
            json = ujson.load(f)

            if not isinstance(json, dict):
                raise ValueError('Malformed JSON in %s: Object expected at top-level.' % path)

            use_icons = cls._json_get_bool(path, json, 'use_icons', '"use_icons"', True)
            show_home = cls._json_get_bool(path, json, 'show_home', '"show_home"', True)

            if 'app_themes' in json.keys():
                cls._json_assert_dict(path, json, 'app_themes')
                app_themes = {}
                for name, theme in json['app_themes'].items():
                    app_themes[name] = cls._json_parse_theme(path, '"app_themes"."%s"' % name, theme)
            else:
                app_themes = None

            if 'default_themes' in json.keys():
                cls._json_assert_list(path, json, 'default_themes')
                default_themes = []
                for theme in json['default_themes']:
                    default_themes.append(cls._json_parse_theme(path, '"default_themes"[]', theme))
            else:
                default_themes = None

            return LauncherConfig(
                use_icons=use_icons,
                show_home=show_home,
                app_themes=app_themes,
                default_themes=default_themes,
            )

    @classmethod
    def _json_get_bool(cls, path, json, field, full_field, default):
        if field in json.keys():
            value = json[field]
            if isinstance(value, bool):
                return value
            else:
                raise ValueError('Malformed JSON in %s: Field %s should be a boolean.' % (path, full_field))
        else:
            return default

    @classmethod
    def _json_assert_dict(cls, path, json, field):
        if field in json.keys() and not isinstance(json[field], dict):
            raise ValueError('Malformed JSON in %s: Field "%s" should be an object.' % (path, field))

    @classmethod
    def _json_assert_list(cls, path, json, field):
        if field in json.keys() and not isinstance(json[field], list):
            raise ValueError('Malformed JSON in %s: Field "%s" should be a list.' % (path, field))

    @classmethod
    def _json_parse_theme(cls, path, full_field, theme):
        icon_str = cls._json_get_str(path, theme, 'icon', '"%s"."icon"' % full_field, None)
        if icon_str:
            if icon_str in ICONS.keys():
                icon = icon_str
            else:
                raise ValueError(
                    'Malformed JSON in %s: Field "%s"."icon" is not a known icon: %s' % (path, full_field, icon_str)
                )
        else:
            icon = None

        fg = cls._json_get_color(path, theme, 'fg', '"%s"."fg"' % full_field, None)
        bg = cls._json_get_color(path, theme, 'bg', '"%s"."bg"' % full_field, None)

        return AppTheme(icon, fg, bg)

    @classmethod
    def _json_get_str(cls, path, json, field, full_field, default):
        if field in json.keys():
            value = json[field]
            if isinstance(value, str):
                return value
            else:
                raise ValueError('Malformed JSON in %s: Field %s should be a string.' % (path, full_field))
        else:
            return default

    @classmethod
    def _json_get_color(cls, path, json, field, full_field, default):
        col_str = cls._json_get_str(path, json, field, full_field, None)
        if col_str:
            if len(col_str) == 7 and cls._HTML_COLOR_REGEXP.match(col_str):
                r = int(col_str[1:3], 16)
                g = int(col_str[3:5], 16)
                b = int(col_str[5:7], 16)
                return color.Color(r, g, b)
            else:
                raise ValueError(
                    'Malformed JSON in %s: Field %s should be a RGB color in HTML format: %s' % (
                        path, full_field, col_str
                    ))
        else:
            return default

    @classmethod
    def default(cls):
        return LauncherConfig(
            app_themes={
                'Home': AppTheme(
                    icon='fairydust_communication',
                    fg=color.COMMYELLOW_DARK,
                    bg=color.COMMYELLOW
                ),
                'Bluetooth': AppTheme(
                    icon='bluetooth',
                    fg=color.WHITE,
                    bg=color.CHAOSBLUE_DARK
                ),
                'CYBER!!1einself!': AppTheme(
                    icon='cyber',
                    fg=color.BLACK,
                    bg=color.COMMYELLOW
                ),
                'DigiClk': AppTheme(
                    icon='digi_clock',
                    fg=color.WHITE,
                    bg=color.CAMPGREEN_DARK
                ),
                'ECG': AppTheme(
                    icon='ecg',
                    fg=color.WHITE,
                    bg=color.CAMPGREEN_DARK
                ),
            }
        )
