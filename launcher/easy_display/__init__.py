import display

FONT8 = display.FONT8
FONT8_WIDTH = 3
FONT8_HEIGHT = 8

FONT12 = display.FONT12
FONT12_WIDTH = 7
FONT12_HEIGHT = 12

FONT16 = display.FONT16
FONT16_WIDTH = 11
FONT16_HEIGHT = 16

FONT20 = display.FONT20
FONT20_WIDTH = 14
FONT20_HEIGHT = 20

FONT24 = display.FONT24
FONT24_WIDTH = 17
FONT24_HEIGHT = 24


class EasyDisplay:
    WIDTH = 160
    HEIGHT = 80

    def __init__(self, dsp):
        self.display = dsp

    def __enter__(self):
        return self

    def __exit__(self, _et, _ev, _t):
        self.close()

    @classmethod
    def open(cls):
        return cls(display.open())

    def close(self):
        self.display.close()

    def update(self):
        self.display.update()

    def clear(self, col=None):
        self.display.clear(col)
        return self

    @staticmethod
    def font_width(font):
        if font == FONT8:
            return FONT8_WIDTH

        if font == FONT12:
            return FONT12_WIDTH

        if font == FONT16:
            return FONT16_WIDTH

        if font == FONT20:
            return FONT20_WIDTH

        if font == FONT24:
            return FONT24_WIDTH

        raise IOError('EasyDisplay does not know about this font: %s' % font)

    @staticmethod
    def font_height(font):
        if font == FONT8:
            return FONT8_HEIGHT

        if font == FONT12:
            return FONT12_HEIGHT

        if font == FONT16:
            return FONT16_HEIGHT

        if font == FONT20:
            return FONT20_HEIGHT

        if font == FONT24:
            return FONT24_HEIGHT

        raise IOError('EasyDisplay does not know about this font: %s' % font)

    def print(self, text, *, fg=None, bg=None, posx=0, posy=0, font=display.FONT20):
        return self.display.print(text, fg=fg, bg=bg, posx=posx, posy=posy, font=font)

    def pixel(self, x, y, *, col=None):
        ix = int(x)
        iy = int(y)
        if 0 <= ix < self.WIDTH and 0 <= iy < self.HEIGHT:
            self.display.pixel(ix, iy, col=col)

        return self

    def backlight(self, brightness):
        self.display.backlight(max(0, min(int(brightness), 100)))
        return self

    @staticmethod
    def _bound_size(size):
        return max(1, min(size, 8))

    def _in_bounds(self, x, y):
        return self._in_x_bounds(x) and self._in_y_bounds(y)

    def _in_x_bounds(self, x):
        return 0 <= x < self.WIDTH

    def _in_y_bounds(self, y):
        return 0 <= y < self.HEIGHT

    def line(self, xs, ys, xe, ye, *, col=None, dotted=False, size=1):
        if xs == xe:
            if ys > ye:
                ys, ye = ye, ys

            def bound_point(x, y):
                if y < 0:
                    y = min(0, ye)
                elif y >= self.HEIGHT:
                    y = max(ys, self.HEIGHT - 1)

                return int(x), int(y)

        elif ys == ye:
            if xs > xe:
                xs, xe = xe, xs

            def bound_point(x, y):
                if x < 0:
                    x = min(0, xe)
                elif x >= self.WIDTH:
                    x = max(xs, self.WIDTH - 1)

                return int(x), int(y)

        else:
            if xs > xe:
                xs, xe = xe, xs
                ys, ye = ye, ys

            bys, bye = ys, ye
            if bys > bye:
                bys, bye = bye, bys

            m = (ye - ys) / (xe - xs)
            b = ys - m * xs

            def f(x):
                return m * x + b

            def fs(y):
                return (y - b) / m

            def bound_point(x, y):
                (bx, by) = (x, y)

                if bx < 0:
                    bx = min(0, xe)
                    by = f(bx)

                elif bx >= self.WIDTH:
                    bx = max(xs, self.WIDTH - 1)
                    by = f(bx)

                if by < 0:
                    by = min(0, bye)
                    bx = fs(by)
                elif by >= self.HEIGHT:
                    by = max(bys, self.HEIGHT - 1)
                    bx = fs(by)

                return int(bx), int(by)

        (bxs, bys) = bound_point(xs, ys)
        (bxe, bye) = bound_point(xe, ye)

        if self._in_bounds(bxs, bys) and self._in_bounds(bxe, bye):
            self.display.line(bxs, bys, bxe, bye, col=col, dotted=dotted, size=self._bound_size(size))

        return self

    def rect(self, xs, ys, xe, ye, *, col=None, filled=True, size=1):
        def to_bounded_x(x):
            return max(0, min(x, self.WIDTH - 1))

        def to_bounded_y(y):
            return max(0, min(y, self.HEIGHT - 1))

        b_size = self._bound_size(size)

        ixs = int(xs)
        iys = int(ys)
        ixe = int(xe)
        iye = int(ye)

        if ixs > ixe:
            (ixs, ixe) = (ixe, ixs)

        if iys > iye:
            (iys, iye) = (iye, iys)

        bxs = to_bounded_x(ixs)
        bys = to_bounded_y(iys)
        bxe = to_bounded_x(ixe)
        bye = to_bounded_y(iye)

        x_intersects = ixs < self.WIDTH and ixe >= 0
        y_intersects = iys < self.HEIGHT and iye >= 0

        if x_intersects and y_intersects:
            if filled:
                self.display.rect(bxs, bys, bxe, bye, col=col, filled=True, size=b_size)

            else:
                if self._in_x_bounds(ixs):
                    self.display.line(bxs, bys, bxs, bye, col=col, size=b_size)

                if self._in_x_bounds(ixe):
                    self.display.line(bxe, bys, bxe, bye, col=col, size=b_size)

                if self._in_y_bounds(iys):
                    self.display.line(bxs, bys, bxe, bys, col=col, size=b_size)

                if self._in_y_bounds(iye):
                    self.display.line(bxs, bye, bxe, bye, col=col, size=b_size)

        return self

    # def circ(self, x, y, rad, *, col=None, filled=True, size=1):
    #     raise NotImplementedError('EasyDisplay.circ() is not yet implemented.')
