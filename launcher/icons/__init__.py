import os

ICONS = {}


def __init__():
    for name in os.listdir('/launcher/icons'):
        if name == '__init__.py' or not name.endswith('.py'):
            continue

        module_name = name[:-3]
        ICONS[module_name] = __import__(module_name, None, None, (), 1).ICON


__init__()
