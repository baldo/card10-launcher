from launcher.polygon import Polygon

_INNER_BOTTOM = Polygon(
    (3, 6),
    (3, 16),
    (8, 11),
)

ICON = [
    Polygon(  # outer
        (15, -11),
        (-2, -28),
        (-2, -5),
        (-12, -15),
        (-15, -12),
        (-3, 0),
        (-15, 12),
        (-12, 15),
        (-2, 5),
        (-2, 28),
        (15, 11),
        (4, 0),
    ),
    _INNER_BOTTOM,
    _INNER_BOTTOM.mirror_y()
]
