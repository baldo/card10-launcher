from launcher.polygon import Polygon

_H_BAR = Polygon(
    (-7, -1),
    (7, -1),
    (8, 0),
    (-8, 0),
    (-7, 1),
    (7, 1),
    closed=False
)
_V_BAR = Polygon(
    (0, -8),
    (0, 8),
    (-1, 7),
    (-1, -7),
    (1, -7),
    (1, 7),
    closed=False
)

ICON = [
    # 2
    _H_BAR.translate(0 - 45, -20),
    _H_BAR.translate(0 - 45, 0),
    _H_BAR.translate(0 - 45, 20),
    _V_BAR.translate(10 - 45, -10),
    _V_BAR.translate(-10 - 45, 10),

    # 3
    _H_BAR.translate(0 - 18, -20),
    _H_BAR.translate(0 - 18, 0),
    _H_BAR.translate(0 - 18, 20),
    _V_BAR.translate(10 - 18, -10),
    _V_BAR.translate(10 - 18, 10),

    # :
    Polygon(
        (-1, -1),
        (1, -1),
        (1, 0),
        (-1, 0),
        (-1, 1),
        (1, 1),
        offset_y=-6,
        closed=False
    ),
    Polygon(
        (-1, -1),
        (1, -1),
        (1, 0),
        (-1, 0),
        (-1, 1),
        (1, 1),
        offset_y=6,
        closed=False
    ),

    # 4
    _H_BAR.translate(0 + 18, 0),
    _V_BAR.translate(-10 + 18, -10),
    _V_BAR.translate(10 + 18, -10),
    _V_BAR.translate(10 + 18, 10),

    # 2
    _H_BAR.translate(0 + 45, -20),
    _H_BAR.translate(0 + 45, 0),
    _H_BAR.translate(0 + 45, 20),
    _V_BAR.translate(10 + 45, -10),
    _V_BAR.translate(-10 + 45, 10),
]
