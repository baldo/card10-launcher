from launcher.polygon import Polygon

_FAIRYDUST_HALF = Polygon(
    (-10, 7),
    (-6, 12),
    (-8, 20),
    (-10, 7),
    (-5, 1),
    (-6, 12),
    (-3, 7),
    (-5, 1),
    (0, 0),
    (-3, 7),
    (0, 20),
    (0, 0),
    (-5, -14),
    (-5, 1),
    (-7, -15),
    (-5, -14),
    (0, -18),
    (-7, -15),
    (0, -23),
    (0, -18),
    (-5, -14),
    (0, -10),
    (0, 0),
    closed=False,
    scale=1.4
)

ICON = [
    _FAIRYDUST_HALF,
    _FAIRYDUST_HALF.mirror_x()
]
