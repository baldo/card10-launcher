import math
import os
import sys

import buttons
import color
import utime

from launcher.apps import AppTheme
from launcher.easy_display import EasyDisplay, FONT16
from launcher.icons import ICONS

FALLBACK_APP_THEME = AppTheme(fg=color.WHITE, bg=color.CHAOSBLUE_DARK)

DISPLAY_W = EasyDisplay.WIDTH
DISPLAY_H = EasyDisplay.HEIGHT

# TODO: Move to config
FONT = FONT16
FONT_W = EasyDisplay.font_width(FONT)
FONT_H = EasyDisplay.font_height(FONT)

# TODO: Move to config
PADDING_X = FONT_W
PADDING_Y = 3

# TODO: Move to config
ARROW_INACTIVE_COLOR = color.from_hex(0x333333)
ARROW_ACTIVE_COLOR = color.from_hex(0x666666)

# TODO: Move to config
TRANSITION_DURATION_MS = 300
MAX_INACTIVITY_MS = 20 * 1000


class MainMenu:
    def __init__(self, apps, config):
        self._apps = []
        self._home = None

        for app in apps:
            config.apply_theme(app)

            if app.is_home and not config.show_home:
                self._home = app
                continue

            self._apps.append(app)

        if not self._apps and self._home:
            # Fallback in edge case of no apps: Show at least home app.
            self._apps = [self._home]

        self._config = config

        self._active_app_index = 0

        self._transition_direction = 0
        self._transition_index = None
        self._transition_color = None
        self._transition_start_time = None

        self._last_active = None

    def run(self):
        self._last_active = utime.time_ms()

        while True:
            self._update()

    def _active_app(self):
        return self._apps[self._active_app_index]

    def _transition_app(self):
        return self._apps[self._transition_index]

    def _render_icon(self, d, icon, color, offset_x):
        polys = ICONS[icon]

        cx = int(DISPLAY_W / 2 + offset_x)
        cy = int(DISPLAY_H / 2)

        for poly in polys:
            poly.draw(d, cx, cy, col=color)

    def _render_app(self, d, app, offset_x=0):
        theme = app.theme or FALLBACK_APP_THEME

        fg = theme.fg or FALLBACK_APP_THEME.fg
        bg = theme.bg or FALLBACK_APP_THEME.bg

        rx0 = offset_x + PADDING_X / 2
        ry0 = 0
        rx1 = offset_x + (DISPLAY_W - 1) - PADDING_X / 2
        ry1 = DISPLAY_H - 1

        d.rect(rx0, ry0, rx1, ry1, col=bg, filled=True)

        if self._config.use_icons and theme.icon is not None:
            self._render_icon(d, theme.icon, fg, offset_x)
            return

        max_name_width = DISPLAY_W - 2 * PADDING_X
        max_name_chars = math.floor(max_name_width / FONT_W)
        max_lines = math.floor((DISPLAY_H - 2 * PADDING_Y) / FONT_H)

        app_name = app.name

        app_name_lines = []
        remaining_chars = len(app_name)
        offset = 0

        while remaining_chars > 0:
            consumed_chars = 0
            display_chars = 0

            last_whitespace = None
            last_delimiter = None
            last_camel_case_word = None
            last_char = None

            for c in app_name[offset:]:
                if c == ' ':
                    last_whitespace = consumed_chars
                elif c in '.-_!':
                    last_delimiter = consumed_chars
                elif (last_char is None or last_char.islower()) and c.isupper():
                    # New CamelCase word.
                    last_camel_case_word = consumed_chars

                consumed_chars += 1
                display_chars = consumed_chars

                if consumed_chars >= max_name_chars:
                    if last_whitespace is not None and last_whitespace > 0:
                        display_chars = last_whitespace
                        consumed_chars = display_chars + 1
                    elif last_delimiter is not None:
                        display_chars = last_delimiter + 1
                        consumed_chars = display_chars
                    elif last_camel_case_word is not None and last_camel_case_word > 0:
                        display_chars = last_camel_case_word
                        consumed_chars = display_chars

                    break

            app_name_lines.append(app_name[offset: offset + display_chars])

            offset += consumed_chars
            remaining_chars -= consumed_chars

            if len(app_name_lines) >= max_lines:
                break

        y = int((DISPLAY_H - FONT_H * len(app_name_lines)) / 2)

        for i in range(len(app_name_lines)):
            line = app_name_lines[i]
            line_w = FONT_W * len(line)

            x = offset_x + int((DISPLAY_W - line_w) / 2)

            start = 0
            end = len(line)

            if x < 0:
                start = min(math.ceil(-x / FONT_W), end)
                x += start * FONT_W
            elif x + line_w >= DISPLAY_W:
                end = max(end - math.ceil((x + line_w + 1 - DISPLAY_W) / FONT_W), start)

            d.print(
                app_name_lines[i][start: end],
                posx=x,
                posy=y + i * FONT_H,
                font=FONT,
                fg=fg,
                bg=bg
            )

    def _check_buttons(self):
        if self._transition_index is None:
            pressed = buttons.read(buttons.BOTTOM_LEFT | buttons.BOTTOM_RIGHT | buttons.TOP_RIGHT)

            if pressed:
                self._last_active = utime.time_ms()

            if pressed & buttons.BOTTOM_LEFT != 0:
                self._start_transition(-1)

            elif pressed & buttons.BOTTOM_RIGHT != 0:
                self._start_transition(1)

            elif pressed & buttons.TOP_RIGHT != 0:
                self._exec_active_app()

    def _exec_active_app(self):
        self._exec_app(self._active_app())

    def _exec_app(self, app):
        self._exec_path(app.path)

    def _exec_path(self, path):
        with EasyDisplay.open() as d:
            d.clear().update()

        try:
            print("Trying to load " + path)
            os.exec(path)
        except OSError as e:
            print("Loading failed: ")
            sys.print_exception(e)
            # TODO: Show errors to the user.
            # self.error("Loading", "failed")
            utime.sleep(1.0)
            os.exit(1)

    def _quit_to_main(self):
        with EasyDisplay.open() as d:
            d.clear().update()

        try:
            f = open("main.py")
            f.close()
            os.exec("main.py")
        except OSError:
            self._last_active = utime.time_ms()

    def _start_transition(self, direction):
        self._transition_direction = direction
        self._transition_index = (self._active_app_index + direction) % len(self._apps)
        self._transition_start_time = utime.time_ms()

    def _is_in_transition(self):
        return self._transition_index is not None

    def _end_transition_if_done(self):
        if self._is_in_transition():
            elapsed_time = self._elapsed_transition_time()
            if elapsed_time < TRANSITION_DURATION_MS:
                return

            self._active_app_index = self._transition_index
            self._active_app_color = self._transition_color

            self._transition_direction = 0
            self._transition_index = None
            self._transition_color = None
            self._transition_start_time = None

    def _elapsed_transition_time(self):
        return utime.time_ms() - self._transition_start_time

    def _update(self):
        time_since_last_activity = utime.time_ms() - self._last_active
        if time_since_last_activity > MAX_INACTIVITY_MS:
            self._quit_to_main()

        with EasyDisplay.open() as d:
            d.clear()
            if self._is_in_transition():
                elapsed_time = self._elapsed_transition_time()

                # easing
                progress = elapsed_time / TRANSITION_DURATION_MS
                if progress < 0.5:
                    progress = max(0.0, 4 * progress ** 3)
                else:
                    progress = min((progress - 1) * (2 * progress - 2) * (2 * progress - 2) + 1, 1.0)

                offset_x = int(DISPLAY_W * progress)

                if self._transition_direction > 0:
                    self._render_app(d, self._active_app(), offset_x=-offset_x)
                    self._render_app(d, self._transition_app(), offset_x=DISPLAY_W - offset_x)
                else:
                    self._render_app(d, self._active_app(), offset_x=offset_x)
                    self._render_app(d, self._transition_app(), offset_x=-DISPLAY_W + offset_x)
            else:
                self._render_app(d, self._active_app())

            d.rect(0, 0, PADDING_X - 1, DISPLAY_H, col=color.BLACK, filled=True)
            d.rect(DISPLAY_W - PADDING_X, 0, DISPLAY_W, DISPLAY_H, col=color.BLACK, filled=True)

            arrow_w = PADDING_X - 6
            arrow_cy = DISPLAY_H - 10 - arrow_w + arrow_w / 2
            run_cy = arrow_cy - arrow_w - 10

            left_arrow_color = ARROW_INACTIVE_COLOR
            right_arrow_color = ARROW_INACTIVE_COLOR
            run_arrow_color = ARROW_INACTIVE_COLOR
            if self._is_in_transition():
                if self._transition_direction < 0:
                    left_arrow_color = ARROW_ACTIVE_COLOR
                else:
                    right_arrow_color = ARROW_ACTIVE_COLOR

            d.line(2, arrow_cy, 4, arrow_cy, col=left_arrow_color)
            d.line(DISPLAY_W - 3, arrow_cy, DISPLAY_W - 5, arrow_cy, col=right_arrow_color)
            d.line(DISPLAY_W - 3, run_cy, DISPLAY_W - 3 - arrow_w, run_cy, col=run_arrow_color)
            for r in range(1, arrow_w):
                d.line(2 + r, arrow_cy - r, 4 + r, arrow_cy - r, col=left_arrow_color)
                d.line(2 + r, arrow_cy + r, 4 + r, arrow_cy + r, col=left_arrow_color)
                d.line(DISPLAY_W - 3 - r, arrow_cy - r, DISPLAY_W - 5 - r, arrow_cy - r,
                       col=right_arrow_color)
                d.line(DISPLAY_W - 3 - r, arrow_cy + r, DISPLAY_W - 5 - r, arrow_cy + r,
                       col=right_arrow_color)
                d.line(DISPLAY_W - 3 - r, run_cy - r, DISPLAY_W - 3 - arrow_w, run_cy - r,
                       col=run_arrow_color)
                d.line(DISPLAY_W - 3 - r, run_cy + r, DISPLAY_W - 3 - arrow_w, run_cy + r,
                       col=run_arrow_color)

            d.update()

        self._check_buttons()
        self._end_transition_if_done()
