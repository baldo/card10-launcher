import color


class Polygon:
    def __init__(self, *points, closed=True, filled=False, scale=1, offset_x=0, offset_y=0):
        self._points = list(points)
        self._closed = closed or filled
        self._filled = filled
        self._scale = scale
        self._offset_x = offset_x
        self._offset_y = offset_y

    @staticmethod
    def _transform_point(p, dx, dy, scale):
        return int(dx + scale * p[0]), int(dy + scale * p[1])

    @staticmethod
    def _line_points(x0, y0, x1, y1):
        if x0 == x1:
            if y0 > y1:
                y0, y1 = y1, y0

            return [(x0, y) for y in range(y0, y1 + 1)]

        if x0 > x1:
            x0, y0, x1, y1 = x1, y1, x0, y0

        if y0 == y1:
            return [(x, y0) for x in range(x0, x1 + 1)]

        m = (y1 - y0) / (x1 - x0)
        n = y0 - m * x0

        return [(x, int(m * x + n)) for x in range(x0, x1 + 1)]

    def draw(self, dsp, x, y, col=None, scale=1):
        if self._closed:
            points = self._points
            prev_point = self._points[-1]
        else:
            points = self._points[1:]
            prev_point = self._points[0]

        total_scale = self._scale * scale

        dx = x + self._offset_x
        dy = y + self._offset_y

        prev_point = self._transform_point(prev_point, dx, dy, total_scale)

        if self._filled:
            print('==============================================================')
            poly_points = {}

            for point in points:
                point = self._transform_point(point, dx, dy, total_scale)

                x = point[0]
                y = point[1]

                dsp.line(x, y, prev_point[0], prev_point[1], col=color.RED)
                print()
                print(x, y, prev_point[0], prev_point[1], self._line_points(x, y, prev_point[0], prev_point[1]))
                print()
                for lx, ly in self._line_points(x, y, prev_point[0], prev_point[1]):
                    if ly not in poly_points:
                        poly_points[ly] = []

                    poly_points[ly].append(lx)

                prev_point = point

            for y in poly_points.keys():
                xs = poly_points[y]
                xs.sort()

                px = xs[0]
                for x in xs[1:]:
                    # if px == x:
                    #     continue

                    dsp.pixel(x, y, col=col)
                    px = x
        else:
            for point in points:
                point = self._transform_point(point, dx, dy, total_scale)

                dsp.line(
                    prev_point[0],
                    prev_point[1],
                    point[0],
                    point[1],
                    col=col)
                prev_point = point

    def mirror_x(self):
        points = []
        for point in self._points:
            points.append((-point[0], point[1]))

        return Polygon(
            *points,
            closed=self._closed,
            scale=self._scale,
            offset_x=self._offset_x,
            offset_y=self._offset_y
        )

    def mirror_y(self):
        points = []
        for point in self._points:
            points.append((point[0], -point[1]))

        return Polygon(
            *points,
            closed=self._closed,
            scale=self._scale,
            offset_x=self._offset_x,
            offset_y=self._offset_y
        )

    def translate(self, dx, dy):
        return Polygon(
            *self._points,
            closed=self._closed,
            scale=self._scale,
            offset_x=self._offset_x + dx,
            offset_y=self._offset_y + dy
        )
